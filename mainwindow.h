#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "dockwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    virtual void moveEvent(QMoveEvent* e) override;
    virtual void resizeEvent(QResizeEvent* e) override;
//    virtual bool eventFilter(QObject* watch, QEvent* event) override;
private slots:
    void on_horizontalSlider_valueChanged(int value);

private:
    Ui::MainWindow *ui;

    DockWindow* dw;
};
#endif // MAINWINDOW_H
