#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QPropertyAnimation>
#include <QGraphicsView>
#include <QLabel>
#include <QTextEdit>
#include <QtGlobal>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    dw = new DockWindow(this);
    dw->SetSize(QSize(120, 400));
    dw->SetStickArea(StickArea::kLRSide);

    dw->AddWidget(new QGraphicsView());
    dw->AddWidget(new QGraphicsView());
    dw->AddWidget(new QGraphicsView());
    dw->AddWidget(new QGraphicsView());
    dw->AddWidget(new QLabel("Hi Dock"));
    dw->AddWidget(new QTextEdit());
//    dw->resize(120, 400);
    auto pat = dw->palette();
    pat.setColor(QPalette::Window, Qt::darkGreen);
    dw->setPalette(pat);
//    dw->SetSize(QSize(200, 400));
//    dw->show();
//    dw->SetPosition(QPoint(0, height()/2 - dw->height()/2));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::moveEvent(QMoveEvent *e)
{
//    dw->SetPosition(QPoint(0, height()/2 - dw->height()/2));

    return QMainWindow::moveEvent(e);
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
    dw->Update();

    QMainWindow::resizeEvent(e);
}



void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    dw->SetOpacity(value);
}

